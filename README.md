# ExtensionLookup
An API for looking up file extensions.

## Lookup

### Get Every Extension
To lookup every extension just create a GET request to the following URL: `http://lambocreeper.uk/extensionlookup`.

### Get a Specific Extension
To lookup a specific extension just create a GET request to the following URL: `http://lambocreeper.uk/extensionlookup`, however, this time specific a URL query of `?e` with a parameter of the extension you specific. For example, `http://lambocreeper.uk/extensionlookup?e=js`.
